type action =
  | AddNode((float, float))
  | DeleteNode(ReactDigraph.node)
  | RenameNode((ReactDigraph.node, string))
  | SelectNode(option(ReactDigraph.node))
  | UpdateNode(ReactDigraph.node)
  | AddEdge((ReactDigraph.node, ReactDigraph.node))
  | DeleteEdge(ReactDigraph.edge)
  | SelectEdge(option(ReactDigraph.edge))
;

type state = {
  nodes: list(ReactDigraph.node),
  edges: list(ReactDigraph.edge),
  nextNode: int,
  selectedElement: option(ReactDigraph.element)
};

let testNodes = [
  {"id": 1, "x": 300.00, "y": 250.00, "title": "A", "type": "defaultNode"},
  {"id": 2, "x": 600.00, "y": 250.00, "title": "B", "type": "defaultNode"},
  {"id": 3, "x": 450.00, "y": 400.00, "title": "C", "type": "defaultNode"}
];

let testEdges = [
  {"source": 1, "target": 2, "type": "defaultEdge"},
  {"source": 2, "target": 3, "type": "defaultEdge"}
];

let replaceNode = (id, node, nodes: list(ReactDigraph.node)) => [
  node,
  ...List.filter(n => n##id !== id, nodes)
];

let incoming = (edges, node) => 
  List.filter(e => e##target === node##id, edges)
  |> List.length;
let outgoing = (edges, node) => 
  List.filter(e => e##source === node##id, edges)
  |> List.length;

let component = ReasonReact.reducerComponent("GraphBuilder");

let make = _children => {
  ...component,
  initialState: () => {
    nodes: testNodes,
    edges: testEdges,
    nextNode: List.length(testNodes) + 1,
    selectedElement: None,
  },
  reducer: (action, state) =>
    switch action {
    | AddNode((x, y)) => {
      let newNode = {
        "id": state.nextNode + 1,
        "x": x,
        "y": y,
        "title": "asdf",
        "type": "defaultNode"
      };

      ReasonReact.Update({
        ...state,
        selectedElement: Some(ReactDigraph.Node(newNode)),
        nodes: [
          newNode,
          ...state.nodes
        ],
        nextNode: state.nextNode + 1
      })
    }
    | RenameNode((node, name)) =>
      ReasonReact.Update({
        ...state,
        nodes: replaceNode(node##id, Js.Obj.assign(node, { "title": name }), state.nodes)
      })
    | SelectNode(node) => 
      ReasonReact.Update({
        ...state,
        selectedElement: switch(node) { | Some(n) => Some(ReactDigraph.Node(n)) | None => None }
      })
    | UpdateNode(node) =>
      ReasonReact.Update({
        ...state,
        nodes: replaceNode(node##id, node, state.nodes)
      })
    | DeleteNode(node) =>
      ReasonReact.Update({
        ...state,
        edges: List.filter(e => e##source !== node##id && e##target !== node##id, state.edges),
        nodes: List.filter(n => n##id !== node##id, state.nodes)
      })
    | AddEdge((srcNode, targetNode)) =>
      ReasonReact.Update({
        ...state,
        selectedElement: None,
        edges: [
          {
            "source": srcNode##id,
            "target": targetNode##id,
            "type": "defaultEdge"
          },
          ...state.edges
        ]
      })
    | DeleteEdge(edge) =>
      ReasonReact.Update({
        ...state,
        edges: List.filter(e => !(e##source === edge##source && e##target === edge##target), state.edges)
      })
    | SelectEdge(edge) =>
      ReasonReact.Update({
        ...state,
        selectedElement: switch(edge) { | Some(e) => Some(ReactDigraph.Edge(e)) | None => None }
      })
    },
  render: self =>
    <div style=(ReactDOMRe.Style.make(~display="flex", ()))>
      <div style=(ReactDOMRe.Style.make(~height="700px", ~width="700px", ()))>
        <h2>(ReasonReact.stringToElement("Draw a Graph"))</h2>
        <ReactDigraph
          nodes=self.state.nodes
          edges=self.state.edges
          selected=self.state.selectedElement
          onNodeCreated=(self.reduce(coordinates => AddNode(coordinates)))
          onNodeSelected=(self.reduce(viewNode => SelectNode(viewNode)))
          onNodeUpdated=(self.reduce(viewNode => UpdateNode(viewNode)))
          onNodeDeleted=(self.reduce(viewNode => DeleteNode(viewNode)))
          onEdgeCreated=(self.reduce(nodePair => AddEdge(nodePair)))
          onEdgeSelected=(self.reduce(viewEdge => SelectEdge(viewEdge)))
          onEdgeDeleted=(self.reduce(viewEdge => DeleteEdge(viewEdge)))
        />
      </div>
      <div style=(ReactDOMRe.Style.make(~paddingLeft="10px", ()))>
        (
          switch (self.state.selectedElement) {
          | Some(Node(n)) => 
            <div>
              <h2>(ReasonReact.stringToElement("Node Properties"))</h2>
              <p>(ReasonReact.stringToElement("Title: " ++ n##title))</p>
              <p>(ReasonReact.stringToElement("ID: " ++ string_of_int(n##id)))</p>
              <p>(ReasonReact.stringToElement("Incoming Edges: " ++ string_of_int(incoming(self.state.edges, n))))</p>
              <p>(ReasonReact.stringToElement("Outgoing Edges: " ++ string_of_int(outgoing(self.state.edges, n))))</p>
              <GraphControls selectedNode=(n) renameNode=(self.reduce(title => RenameNode((n, title)))) />
            </div>
          | _ => <div />
          }
        )
      </div>
    </div>
};