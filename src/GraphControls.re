type state = {
  nodeName: string
};

type action =
  | SetNewNodeName(string)
;

let component = ReasonReact.reducerComponent("GraphControls");

let make = (~selectedNode: ReactDigraph.node, ~renameNode, _children) => {
  let renameNodeTime = (_event, self: ReasonReact.self('state, 'payload, 'action)) => {
    renameNode(self.state.nodeName);
  };

  {
    ...component,
    initialState: () => { nodeName: selectedNode##title },
    reducer: (action, _state) => {
      switch (action) {
      | SetNewNodeName(name) => ReasonReact.Update({ nodeName: name })
      }
    },
    render: (self) => {
      <div>
        <div>
          <h2>{ReasonReact.stringToElement("Rename Node")}</h2>
          <input onChange={self.reduce(event => SetNewNodeName(ReactDOMRe.domElementToObj(ReactEventRe.Form.target(event))##value))} _type="text" placeholder=(selectedNode##title) />
          <button onClick={self.handle(renameNodeTime)}>{ReasonReact.stringToElement("Rename")}</button>
        </div>
      </div>
    }
  }
};
