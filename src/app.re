let component = ReasonReact.statelessComponent("app");

let make = (~message, _children) => {
  ...component,
  render: (_self) => {
    <div style=(ReactDOMRe.Style.make(~fontFamily="Arial", ()))> 
      <h1> {ReasonReact.stringToElement(message)} </h1>
      <GraphBuilder />
    </div>
  }
};
