[@bs.module "react-digraph"] external reactDigraph : ReasonReact.reactClass = "default";

type node = {.
  "id": int,
  "x": float,
  "y": float,
  "title": string,
  "type": string
};

type edge = {.
  "source": int,
  "target": int,
  "type": string
};

type element =
  | Node(node)
  | Edge(edge)
;

let highlightSelected = (maybeElement) => {
  switch (maybeElement) {
  | Some(Node(n)) => Js.Obj.assign(n, Js.Obj.empty())
  | Some(Edge(e)) => Js.Obj.assign(e, Js.Obj.empty())
  | None => Js.Obj.empty()
  };
};

let make = (
  ~nodes, 
  ~edges,
  ~selected,
  ~onNodeCreated,
  ~onNodeSelected,
  ~onNodeUpdated,
  ~onNodeDeleted,
  ~onEdgeSelected,
  ~onEdgeCreated,
  ~onEdgeDeleted,
  children
  ) => 
  ReasonReact.wrapJsForReason(
    ~reactClass=reactDigraph,
    ~props={
      /* These props are all required */
      "nodeKey": "id",
      "emptyType": "defaultNode",
      "nodes": nodes |> Array.of_list,
      "edges": edges |> Array.of_list,
      "selected": highlightSelected(selected),
      "nodeTypes": {
        "defaultNode": {
          "textType": "None",
          "shapeId": "#defaultNode",
          "shape": (
            <symbol viewBox="0 0 100 100" id="defaultNode" key="0">
              <circle cx="50" cy="50" r="45"></circle>
            </symbol>
          )
        }
      },
      "nodeSubtypes": Js.Obj.empty(),
      "edgeTypes": {
        "defaultEdge": {
          "shapeId": "#defaultEdge",
          "shape": (
            <symbol viewBox="0 0 50 50" id="defaultEdge" key="0">
              <circle cx="25" cy="25" r="12" fill="currentColor"> </circle>
            </symbol>
          )
        }
      },
      "getViewNode": (nodeKey) => {
        let result = if (List.exists(n => n##id === nodeKey, nodes)) {
          Some(List.find(n => n##id === nodeKey, nodes))
        } else {
          None
        };

        Js.Nullable.from_opt(result);
      },
      "onSelectNode": (viewNode) => onNodeSelected(Js.Nullable.to_opt(viewNode)),
      "onCreateNode": (x: float, y: float) => onNodeCreated((x, y)),
      "onUpdateNode": onNodeUpdated,
      "onDeleteNode": onNodeDeleted,
      "onSelectEdge": viewEdge => onEdgeSelected(Js.Nullable.to_opt(viewEdge)),
      "onCreateEdge": (sourceViewNode, targetViewNode) => onEdgeCreated((sourceViewNode, targetViewNode)),
      "onDeleteEdge": onEdgeDeleted,
      /* These props are all optional */
      "graphControls": false,
      "readOnly": false
    },
    children
  );