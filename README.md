# reason-digraph

A wrapper for the react-digraph library for ReasonML. Provides a wrapper into some of the basic hooks react-digraph (create/remove/select nodes and edges). Also provides a basic implementation to test it out.

![Screenshot of the demo](./public/demo-ss.png)

## Getting Started

1. Clone this repo.
1. Run `npm i` or `yarn` to install any required dependencies.
1. Start the app via `npm start` or `yarn start`.
1. Navigate to http://locahost:3000 to tinker with the basic implementation.

## Some of the controls

Here are a few of the controls I've figured out so far:

1. Add a node - `Shift + Click` in the canvas.
1. Remove a node - `Click` the node, then press `Delete` or `Backspace`.
1. Add an edge - Hold `Shift`. `Click` and hold on the starting node. Drag to the ending node. Release `Shift` and `Click`.
1. Remove an edge - `Click` the edge, then press `Delete` or `Backspace`.
1. Move a node - `Click` and hold the node. Drag it to the location you want. Release `Click`.

## Renaming a node

A bare-bones form exists to allow renaming nodes. The form will appear after selecting a node. The form will disappear after unselecting a node.

1. `Click` the node to rename.
1. Notice the "Rename Node" form at the bottom of the demo.
1. Update the text with the desired name.
1. Click "Rename".

## Quirks

There have been some odd quirks while working through this. Here are some you may pick up on while playing. If you find reasonable ways to fix these, or any mistakes I've made, feel free to let me know!

1. Directly click on a node or edge does not properly select it.

    This appears to come from react-digraph dispatching two events (one where the element is first selected, then the 2nd one indicating that the element was un-selected). [It may be related to this issue](https://github.com/uber/react-digraph/issues/13). A hack is to move the mouse slightly when picking an element. Right-clicking on nodes also appears to work properly

1. The console prints `event is undefined` a lot. Also a bunch of events don't seem to work. Also why is this so broken?

    This appears to come from react-digraph looking for an `event` var from its event handlers. Most of the time, react-digraph [relies on D3's `d3.event` global](https://www.dashingd3js.com/lessons/d3-and-dom-events), but in some handlers it does not. Locally, I've changed these references to use `d3.event` which appears to work fine. This won't fly for an actual app, but for this demo it's OK. Again if you see the mistake I've made somewhere, please let me know.

(This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).)